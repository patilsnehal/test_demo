var express = require("express");
var app = express();
var emailSender = require('./EmailSender');
var cors = require('cors');

app.use(cors())

app.listen(3000, () => {
  console.log("Server running on port 3000");
});

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get("/dummy", (req, res, next) => {
  res.json(["Tony", "Lisa", "Michael", "Ginger", "Food"]);
});

app.post("/superDailyNeed/sendOrderConfirmationEmail", (req, res, next) => {
  console.info("Request received. Processing the data.....")
  var isValid = validate(req.body)
  if (isValid) {
    processValidRequest(req, res);
  } else {
    console.info("Request processing end (Error).")
    res.status(422).json("Unable to process data").end();
  }
});

function processValidRequest(req, res) {
  emailSender.sendOrderConfirmationEmail(req.body);
  console.info("Request processing end (Success).")
  return res.status(200).json("IsValid").end();
}

function validate(body) {
  return body["emailId"] === undefined
    || body["username"] === undefined
    || (body["products"] != undefined && body["products"].length > 0);
}