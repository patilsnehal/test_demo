import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { OffersComponent } from './offers/offers.component';
import { CustomerSupportComponent } from './customer-support/customer-support.component';
import { CartComponent } from './cart/cart.component';
import { CreatAccountComponent } from './creat-account/creat-account.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { ProfileComponent } from './profile/profile.component';

import { ProductComponent } from './product/product.component';
import { AdminComponent } from './admin/admin.component';
import { ViewProductCategoryComponent } from './view-product-category/view-product-category.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { UsersComponent } from './users/users.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { MyordersComponent } from './myorders/myorders.component';
import { MyordersproductComponent } from './myordersproduct/myordersproduct.component';
import { TryComponent } from './try/try.component';
import { DeliveredorderComponent } from './deliveredorder/deliveredorder.component';
import { OrdersComponent } from './orders/orders.component';


const routes: Routes = [

  { path: '', redirectTo: 'home', pathMatch: 'full'},

  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'offers', component: OffersComponent },
  { path: 'customersupport', component: CustomerSupportComponent },
  { path: 'cart', component: CartComponent },
  { path: 'creataccount', component: CreatAccountComponent },
  { path: 'forgotpass', component: ForgotPassComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'product', component: ProductComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'viewproductcategory', component: ViewProductCategoryComponent },
  { path: 'viewproduct', component: ViewproductComponent },
  { path: 'productlist', component: ProductlistComponent },
  { path: 'users', component: UsersComponent },
  { path: 'checkout', component: CheckoutComponent},
  { path: 'deliveredorder', component: DeliveredorderComponent},
  { path: 'orders', component: OrdersComponent},
  { path: 'myorders', component: MyordersComponent},
  { path: 'myordersproduct', component: MyordersproductComponent},
  { path: 'try', component: TryComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
