import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { EmailSenderManager } from '../util/EmailSenderManager';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  orderId : any;
  today : any;

  public cart: any = [];
  public total_item: any;
  public total_qty = 0;
  public total: any;
  public users: any;
  public fname: any;
  public lname: any;
  public phone: any;
  public address: any;
  public email: any;
  public totalAmt = 0;

  public brand: any;
  public measure: any;
  public name: any;
  public payment_status: any;
  public price: any;
  public product_quantity: any;
  public uid: any;

  constructor(public angFirestore: AngularFirestore,
    public router: Router,
    public route: ActivatedRoute,
    public angularFireAuth: AngularFireAuth,
    public httpClient: HttpClient) { }

  ngOnInit() {
    this.today = formatDate(new Date(),'dd MMM yyyy', 'en-US', '+0530');
    this.orderId = Math.floor((Math.random() * 1000000000) + 100000000);
    this.get_product();
  }
  get_product() {
    this.cart = [];
    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('myOrders',ref=>ref.where('user_uid','==',user.uid)).valueChanges().subscribe(res=>{
          console.log('length : '+ res.length);
          console.log(res);
          this.angFirestore.collection('myOrderProducts',ref=>ref.where('uid','==',user.uid)).valueChanges().subscribe(res1=>{
            console.log('lengthProduct : '+ res1.length);
            console.log(res1);  
          });  
          // res.forEach(data=>{
          //   this.angFirestore.collection('myOrders').doc(data['unique_id']).delete().then(res=>{console.log('Delete All Order');});
          // });
        });
        this.angFirestore.collection('cart', ref => ref.where('payment_status', '==', false)
          .where('uid', '==', user.uid)).valueChanges().subscribe(res => {
            // this.cart = res; 
            let price = 0;
            this.total = 0;
            res.forEach(data => {
              price = 0;
              price = Number(price) + (Number(data['price']) * Number(data['product_quantity']));
              this.total = Number(this.total) + (Number(data['price']) * Number(data['product_quantity']));
              Array.prototype.push.apply(data, [{ total: price }]);
              this.cart.push(data);
            });
            
          this.totalAmt = Number(this.total) + 40;
            this.total_item = res.length;
            //this.total_qty = this.total_qty + (Number(res['total_item'])); 
            console.log('total all items' + this.total_item);

            res.forEach(
              data => {
                this.total_qty = this.total_qty + (Number(data['product_quantity']));
              }
            );

            console.log(res);
            console.log('total all qty' + this.total_qty);
          });

      }
    });

    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).
          valueChanges().subscribe(val => {
            this.users = val;
            console.log(val);
            this.fname = val[0]['firstName'];
            this.lname = val[0]['lastName'];
            this.phone = val[0]['mobileNumber'];
            this.address = val[0]['address'];
            console.log(this.address);
            this.email = val[0]['emailId'];
          });
      }
    });
  } 
  submit_order() {
    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).
          valueChanges().subscribe(val => {
            this.users = val;
            this.address = val[0]['address'];
            if (!this.address.Line_1 && !this.address.Line_2) {
              console.log('add address first');
              let toast = document.getElementById('Addressadd');
              toast.style.display = 'block';
              setTimeout(() => {
                toast.style.display = 'none';
              }, 2000);
            }
            else {
                  const newId1 = this.angFirestore.createId();
                  this.angFirestore.collection('myOrders').doc(newId1).set({
                    unique_id:newId1,
                    timestamp: new Date(),
                    order_id:this.orderId,
                    user_uid:user.uid,
                    totalAmt : this.totalAmt,
                    order_status : false
                  });
                  this.angFirestore.collection('cart', ref => ref.where('payment_status', '==', false).
                    where('uid', '==', user.uid)).valueChanges().subscribe(res => {
                      let cnt = 0;
                      if(cnt === 0){
                          res.forEach(
                            data => {
                              this.angFirestore.collection('myOrderProducts', ref => ref.where('product_id', '==', data['unique_id']).
                                where('user_uid', '==', user.uid)).valueChanges().subscribe(res => {
                                  if(res.length === 0){
                                    const newId2 = this.angFirestore.createId();
                                    this.angFirestore.collection('myOrderProducts').doc(newId2).set({
                                      brand: data['brand'],
                                      measure: data['measure'],
                                      image: data['image'],
                                      name: data['name'],
                                      price: data['price'],
                                      product_quantity: data['product_quantity'],
                                      user_uid: user.uid,
                                      order_status: 'Undelivered',
                                      product_id:data['unique_id'],
                                      order_id:newId1,
                                      unique_id: newId2
                                    }).then(res => {
                                      this.angFirestore.collection('cart').doc(data['unique_id']).delete().then(res1=>{
                                        console.log("Delete All Product");
                                      }).catch(err=>{
                                     
                                        console.log(err);
                                      });
                                    });
                                  }  
                                });  
                              cnt++;    
                            }
                          );
                          }  
                    });
              let toast = document.getElementById('orderplace');
              toast.style.display = 'block';
              setTimeout(() => {
                toast.style.display = 'none';
              }, 4000);

              // Send product confirmation email
              let products = [];
              this.cart.forEach(data => {
                let product = {
                  "productName": data.name,
                  "quantity": data.product_quantity,
                  "productPrice": data.price
                }
                products.push(product);
              })
              let emailbody = {
                "username": this.fname + ' ' + this.lname,
                "emailId": this.email,
                "total": this.totalAmt,
                "date": formatDate(new Date(), 'dd MMM yyyy', 'en-US', '+0530'),
                "address": this.address.Line_1 + ', ' + this.address.Line_2 + ', ' + this.address.City + '-' + this.address.Pincode,
                "products": products
              }
              EmailSenderManager.sendOrderConfirmationEmail(this.httpClient, emailbody);
            }
          });
      }

      setTimeout(() => {
        window.location.reload();
      }, 3000);

    });


    // this.angularFireAuth.auth.onAuthStateChanged(user => {
    //   if (user) {
    //     this.angFirestore.collection('cart', ref => ref.where('payment_status', '==', false).
    //     where('uid', '==', user.uid)).valueChanges().subscribe(res => {
    //       res.forEach(
    //         data => {
    //           const newId = this.angFirestore.createId();
    //           this.angFirestore.collection('myOrders').doc(newId).set({
    //             brand: data['brand'],
    //             measure: data['measure'],
    //             name: data['name'],
    //             price: data['price'],
    //             product_quantity: data['product_quantity'],
    //             uid: data['uid'],
    //             order_status: 'Undelivered',
    //             unique_id: newId
    //           }).then(res => {
    //             console.log(res);
    //           });
    //         }
    //       );
    //       res.forEach(data=>{
    //         this.angFirestore.collection('cart').doc(data['unique_id']).delete()
    //         .then(res=>{
    //           console.log('Delete Product');
    //         });
    //       });
    //     });
    //   }
    // });

  }

}
