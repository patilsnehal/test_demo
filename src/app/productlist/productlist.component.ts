import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from 'angularfire2/storage';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {

  public item=[];
  public unique_id:any;
  public section: any;
  product : any =[]; 
  public image: any;
  public name: any;

  constructor(
    public auth: AngularFireAuth,
    public angfstorage: AngularFireStorage,
    public route: ActivatedRoute,
    public angFirestore: AngularFirestore,
    public router: Router
  ) { }

   ngOnInit() {

     window.setInterval(function(){
      setTimeout(() => {
        document.getElementById('nextBtn').click();
      }, 5000); 
    }, 5000);

    this.route.queryParams.subscribe((res) => {
      this.unique_id = res['unique_id'];
      this.section = res['section'];
      console.log(this.unique_id + " " + this.section);
    });
    this.get_product();
    
    this.angFirestore.collection('Product_Category',ref=>ref.where('unique_id', '==', this.unique_id))
    .doc(this.unique_id).collection('ProductCategory').valueChanges().subscribe(val=>{
      this.image = val[0]['image'];
      this.name =  val[0]['name'];
    });

  }

  get_product() {
    this.item=[];
    this.angFirestore.collection('Product_Category').doc(this.unique_id).
      collection('ProductCategory').valueChanges().subscribe(res => {
        this.item = res;
        console.log(res);
      });
  }
 
  viewProduct(id: any,name : any) {
    this.router.navigate(['/product'], {
      queryParams: { unique_id: id,product_unique_id: this.unique_id },
    });
  }

} 
