import { Component, OnInit } from '@angular/core';
import { Router,  ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
//import { get } from 'https';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  address: any;
  public isLoginIn:boolean = false;
  public total_item = 0;
  public cart: any;
  public unique_id: any;
  public shopping_cart: any;
  public cart_count: any;
  public product_quantity:any;
  public payment_status: any;
  public uid:any;
  total = 0;
  //item ={price:''} jevha tu payment vr click karel tevha te cart madhil product update zal pahije paoyment_status: trueok


  constructor(public router: Router, 
              public angularFireAuth: AngularFireAuth,
              public angFirestore: AngularFirestore,
              public route: ActivatedRoute
              ) { }

  ngOnInit() {
    this.angularFireAuth.auth.onAuthStateChanged(user=>{
      if(user) {
        this.isLoginIn = true;
        console.log(user);
        this.uid=user.uid;
        this.angFirestore.collection('newAccount', ref=>ref.where('emailId', '==', user.email)).valueChanges().subscribe(val=>{
          console.log(val);
          this.address = val[0]['address'];
        });
      }else{
        this.isLoginIn = false; 
      }
    }) 
    this.get_product();
  }
  
  get_product() {
    this.cart = [];
    this.total_item = 0;
    this.total=0;
    console.log(this.uid);
    this.angularFireAuth.auth.onAuthStateChanged(user=>{
      if(user) {
          this.angFirestore.collection('cart',ref=>ref.where('payment_status','==',false).where('uid','==',user.uid)).valueChanges().subscribe(res => {
              this.cart = res;
              this.total_item = res.length;
              console.log(res);
              res.forEach(
                    data=>{
                    
                      this.total = this.total + ((Number(data['price'])) * (Number(data['product_quantity'])));
                    }
                  );
              console.log("total"+this.total);
              console.log("product_quantity"+this.product_quantity);
            });
      }
    });  

      
  }
   
  update(id,qty){
    this.angFirestore.collection('cart').doc(id).update({product_quantity:qty}).then(res=>{
      console.log('Update Product Qty : '+qty);
      this.get_product();
    });
  }
  add(id){
    let Qty = 0;
    let cnt = 0; 
    this.angFirestore.collection('cart',ref=>ref.where('unique_id','==',id)).valueChanges().subscribe(res=>{
      Qty = Number(res[0]['product_quantity']) + 1;
      cnt++;
      if(cnt === 1){
        console.log('Update Product Qty : '+ Qty);
        this.update(id,Qty);
      }  
    });
  }

  removeproduct(id){
    console.log("id   "+id);
    this.unique_id = id;
    this.remove(this.unique_id);
  }

  remove(id) {
    let Qty = 1;
    let cnt = 1; 
    this.angFirestore.collection('cart',ref=>ref.where('unique_id','==',id)).valueChanges().subscribe(res=>{
      if( res[0]['product_quantity'] >= 1){
        Qty = Number(res[0]['product_quantity']) - 1;
        cnt--;
        if(cnt === 0){
        this.update(id,Qty);
        }
      }
      else{
        this.angFirestore.collection('cart').doc(id).delete().then(res=>{
            this.get_product();
          });
        }
    });
  }

  deleteAll() {
    this.angFirestore.collection('cart',ref=>ref.where('payment_status','==',false)).valueChanges().subscribe(res => {
      res.forEach(data=>{
        this.delete(data['unique_id']);
        console.log('unique id'+ data['unique_id']);
      }); 
    });
  }

  delete(id) {
    this.angFirestore.collection('cart').doc(id).delete().then(res=>{
      console.log('Delete Product : '+id);
      this.total=0;
    });
  }
  

  checkout(id){
    this.router.navigate(['/checkout']);
  
  }
}
