import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { FormGroup } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ThrowStmt, AstMemoryEfficientTransformer, templateJitUrl } from '@angular/compiler';
//import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-creat-account',
  templateUrl: './creat-account.component.html',
  styleUrls: ['./creat-account.component.scss']
})
export class CreatAccountComponent implements OnInit {

  public firstName="";
  public lastName="";
  public mobileNumber="";
  public emailId="";
  public password="";
  public DOB="";

  constructor(public angFirestore: AngularFirestore, public afAuth: AngularFireAuth, public router: Router) { }
 
  ngOnInit(): void {
  }
  createacc() {
    let plength = new String(this.mobileNumber);
    const format = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.firstName !== '' && this.lastName !== '' && this.mobileNumber !== '' && this.DOB !== ''&& this.emailId !== '' && this.password !== '') 
    { 
      console.log("firstname" + this.firstName);
      if (plength.length !== 10) {
        console.log('plese enter valid 10 digit mobile number');
        let toast = document.getElementById('invalidmobilenum');
        toast.style.display = 'block';
        setTimeout(() => {
          toast.style.display = 'none';
        }, 2000);
      }
      if (!format.test(this.emailId)) {
        console.log('plese enter valid email');
        let toast = document.getElementById('inavlidemail');
        toast.style.display = 'block';
        setTimeout(() => {
          toast.style.display = 'none';
        }, 2000);
      }
      else {
        this.afAuth.auth.createUserWithEmailAndPassword(this.emailId, this.password).then(res => {
          this.afAuth.authState.subscribe(user => {
            if (user) {
              this.angFirestore.collection('newAccount', ref => ref.where('email', '==', this.emailId)).get().subscribe((snapshot) => {
                let val = snapshot.size;
                if (val == 0) {
                  const newId = this.angFirestore.createId();
                  this.angFirestore.collection('newAccount').doc(newId).set({
                    firstName: this.firstName,
                    lastName: this.lastName,
                    mobileNumber: this.mobileNumber,
                    emailId: this.emailId,
                    dob: this.DOB,
                    role: 'user',
                    address: { City: '', Line_1: '', Line_2: '', Pincode: '' },
                    user_uid: user.uid,
                    uid: newId
                  });
                }
              });
            }
          });
          this.router.navigate(['/home']);
        }).catch(err => {
          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
        });
      }
    }
    else {
      console.log('plese enter all field');
      let toast = document.getElementById('filldetails');
        toast.style.display = 'block';
        setTimeout(() => {
          toast.style.display = 'none';
        }, 2000);
    }
  }
}

