import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { BoldPipe } from './bold.pipe';

//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { SharedModule } from './shared';
import { HttpClientModule } from '@angular/common/http'

import { FooterComponent } from './shared/layout/footer.component';
import { HeaderComponent } from './shared/layout/header.component';
import { TopmenuComponent } from './shared/layout/topmenu.component';

import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';

import { OffersComponent } from './offers/offers.component';
import { CustomerSupportComponent } from './customer-support/customer-support.component';
import { CartComponent } from './cart/cart.component';
import { CreatAccountComponent } from './creat-account/creat-account.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { ProfileComponent } from './profile/profile.component';
import { ProductComponent } from './product/product.component';
import { AdminComponent } from './admin/admin.component';
import { ViewProductCategoryComponent } from './view-product-category/view-product-category.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { UsersComponent } from './users/users.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { TryComponent } from './try/try.component';
import { DeliveredorderComponent } from './deliveredorder/deliveredorder.component';
import { OrdersComponent } from './orders/orders.component';
import { MyordersComponent } from './myorders/myorders.component';
import { MyordersproductComponent } from './myordersproduct/myordersproduct.component';
import { from } from 'rxjs';


@NgModule({
  declarations: [ 
    AppComponent,
    LoginComponent,
    DashboardComponent,
    FooterComponent,
    HeaderComponent,
    TopmenuComponent, 
    HomeComponent, 
    OffersComponent, 
    CustomerSupportComponent, 
    CartComponent, 
    CreatAccountComponent, 
    ForgotPassComponent, 
     ProfileComponent, 
     ProductComponent, 
     AdminComponent, 
     ViewProductCategoryComponent, 
     ViewproductComponent, 
     ProductlistComponent, 
     UsersComponent, 
     CheckoutComponent, TryComponent, MyordersComponent, MyordersproductComponent,DeliveredorderComponent, OrdersComponent],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    AppRoutingModule,SharedModule,
    HttpClientModule,
   // ToastrService
  
    BrowserAnimationsModule,
    ReactiveFormsModule,

    // material modules
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private db: AngularFirestore) {
  }
}
