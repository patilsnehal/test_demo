import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from 'angularfire2/storage';
import { map, finalize } from 'rxjs/operators'
import { ImageUploadListener } from '../util/ImageUploadListener';
import { ImageUploadManager } from '../util/ImageUploadManager';


@Component({
  selector: 'app-view-product-category',
  templateUrl: './view-product-category.component.html',
  styleUrls: ['./view-product-category.component.scss']
})
export class ViewProductCategoryComponent implements OnInit, ImageUploadListener {

  public item: any;
  public product_name: any;
  public product_image: any;
  public uid: any;

  public unique_id: any;
  public productId: any;
  public file: any; 
  public section: any;
  public isAddProductRequest;

  constructor(public auth: AngularFireAuth,
    public angfstorage: AngularFireStorage,
    public route: ActivatedRoute,
    public angFirestore: AngularFirestore,
    public router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe((res) => {
      this.unique_id = res['unique_id'];
      this.section = res['section'];
      console.log("Data received " + this.unique_id + " " + this.section);
    });
    this.get_product();
  }

  get_product() {
    this.angFirestore.collection('Product_Category').doc(this.unique_id).
      collection('ProductCategory').valueChanges().subscribe(res => {
        this.item = res;
        console.log(res);
      });
  }

  addproduct() {
    this.isAddProductRequest = true;
    const newId = this.angFirestore.createId();
    console.log(newId);
    if(this.file) {
      this.productId = newId;
      this.uploadProductImage(newId);
    } else {
      this.addProductToFirebase(newId, null);
    }
  }
  
  addProductToFirebase(newId, productCategoryImageUrl: string) {
    this.angFirestore.collection('Product_Category').doc(this.unique_id).
      collection('ProductCategory').doc(newId).set({
        name: this.product_name,
        image: productCategoryImageUrl,
        unique_id: newId
      }).then(res => {
        this.product_name = null;
        this.productId = null;
        this.product_image = null;
        this.file = null;
        this.get_product();
      });
  }

  editimage(event) {
    this.file = event.target.files[0];
    console.log(this.file);
  }

  update(id: any) {
    this.isAddProductRequest = false;
    if (this.file) {
      this.productId = id;
      this.uploadProductImage(id);
    } else {
      this.updateProductData(id, null);
    }
  }

  uploadProductImage(id) {
    ImageUploadManager.uploadImageToFirebase(this.angfstorage,
      this.file,
      this.section + "/ProductCategory/" + id,
      this);
  }

  updateProductData(id: any, productCategoryImageUrl: string) {
    this.angFirestore.collection('Product_Category')
      .doc(this.unique_id).collection('ProductCategory')
      .doc(id).update({
        name: this.product_name,
        image: productCategoryImageUrl
      }).then(res => {
        this.productId = null;
        this.product_name = null;
        this.product_image = null; 
        this.file = null;
      })
  }

  edit(id) {
    this.angFirestore.collection('Product_Category')
      .doc(this.unique_id)
      .collection('ProductCategory', ref => ref.where('unique_id', '==', id))
      .valueChanges().subscribe(res => {
        this.product_name = res[0]['name'],
          this.product_image = res[0]['image'];
        this.uid = res[0]['unique_id'];
      });
  }

  deleteproduct(id) {
    this.productId = id;
    console.log(this.unique_id);
  }

  delete() {
    this.angFirestore.collection('Product_Category').doc(this.unique_id).collection('ProductCategory').doc(this.productId).delete().then(res => {
      console.log('Successfuly data delete');
      this.get_product();
    });
  }

  viewProduct(id) {
    this.router.navigate(['/viewproduct'], {
      queryParams: { unique_id: id, product_unique_id: this.unique_id },
    });
  }

  onSuccess(downloadUrl: string) {
    console.log(downloadUrl);
    if(this.isAddProductRequest) {
      this.addProductToFirebase(this.productId, downloadUrl)
    } else {
      this.updateProductData(this.productId, downloadUrl);
    }
  }

  onError(error: any) {
    console.log(error);
  }
}
