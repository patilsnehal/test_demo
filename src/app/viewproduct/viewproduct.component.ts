import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from 'angularfire2/storage';
import { map, finalize } from 'rxjs/operators'
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageUploadListener } from '../util/ImageUploadListener';
import { ImageUploadManager } from '../util/ImageUploadManager';

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.scss']
})
export class ViewproductComponent implements OnInit, ImageUploadListener {

  public item: any;
  public product_brand: any;
  public product_name: any;
  public product_image: any;
  public product_measure: any;
  public product_price: any;
  public uid: any;
  public unique_id: any;
  public product_unique_id: any;

  public isAddProductRequest: boolean;
  public file: any;
  public productId: any;
  public section: any;

  constructor(public auth: AngularFireAuth,
    public angfstorage: AngularFireStorage,
    public route: ActivatedRoute,
    public angFirestore: AngularFirestore,
    public router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe((res) => {
      this.unique_id = res['unique_id'];
      this.product_unique_id = res['product_unique_id'];
      this.section = res['section'];
      console.log("Data receive " + this.unique_id + ", " + this.product_unique_id + " " + this.section);
      console.log("product_unique_id : " + this.product_unique_id);
    });
    this.get_product();
  }
  
  get_product() {
    console.log("uniqueid" + this.unique_id);
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
      collection('ProductCategory').doc(this.unique_id).
      collection('SubProductCategory').valueChanges().subscribe(res => {
        this.item = res;
        console.log("resss" + res);
      });
  }

  addproduct() {
    this.isAddProductRequest = true;
    const newId = this.angFirestore.createId();
    console.log(newId);
    if (this.file) {
      this.productId = newId;
      this.uploadProductImage(newId);

    }
    else {
      this.addProductToFirebase(newId, null);
    }
  }

  addProductToFirebase(newId, productCategoryImageUrl: string) {
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
      collection('ProductCategory').doc(this.unique_id).
      collection('SubProductCategory').doc(newId).set({
        name: this.product_name,
        brand: this.product_brand,
        image: productCategoryImageUrl,
        measure: this.product_measure,
        price: this.product_price,
        unique_id: newId
      }).then(res => {
        this.product_name = null;
        this.product_brand = null;
        this.product_image = null;
        this.file = null;
        this.product_measure = null;
        this.product_price = null;
        this.productId = null;
        this.get_product();
      });
  }

  editimage(event) {
    this.file = event.target.files[0];
    console.log(this.file);
  }

  uploadProductImage(id) {
    ImageUploadManager.uploadImageToFirebase(this.angfstorage,
      this.file,
      this.section + "/Product/" + id,
      this);
  }
  updateProductData(id: any, productCategoryImageUrl: string) {
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
    collection('ProductCategory').doc(this.unique_id).
    collection('SubProductCategory').doc(id).update({
        name: this.product_name,
        brand:this.product_brand,
        image: productCategoryImageUrl,
        measure:this.product_measure,
        price:this.product_price,
      }).then(res => {
        this.productId = null;
        this.product_name = null;
        this.product_brand = null;
        this.product_image = null;
        this.file = null;
        this.product_measure = null;
        this.product_price = null;
        this.productId = null;        
      })
  }
  
  edit(id) {
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
    collection('ProductCategory').doc(this.unique_id).
    collection('SubProductCategory', ref => ref.where('unique_id', '==', id))
      .valueChanges().subscribe(res => {
        this.product_name = res[0]['name'],
        this.product_brand =res[0]['brand'],
        this.product_measure =res[0]['measure'],
        this.product_price =res[0]['price'],
          this.product_image = res[0]['image'];
        this.uid = res[0]['unique_id'];
      });
  }

  update(id: any) {
    this.isAddProductRequest = false;
    if (this.file) {
      this.productId = id;
      this.uploadProductImage(id);
    } else {
      this.updateProductData(id, null);
    }
  }

  viewProduct(id) {
    this.router.navigate(['/viewproduct'], {
      queryParams: { unique_id: id, product_unique_id: this.unique_id },
    });
  }

  deleteproduct(id) {
    this.productId = id;
    console.log(this.unique_id);
  }

  delete() {
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
    collection('ProductCategory').doc(this.unique_id).
    collection('SubProductCategory').doc(this.productId).delete().then(res => {
      console.log('Successfuly data delete');
      this.get_product();
    });
  }

  // deleteproduct(id) {
  //   this.unique_id = id;
  // }

  // delete(id) {
  //   console.log("deLeeet is" + id);
  //   console.log("uniqieedfid" + this.unique_id);
  //   this.angFirestore.collection('Product_Category').doc(this.unique_id).collection('ProductCategory').doc(id).delete();
  // }

  onSuccess(downloadUrl: string) {
    console.log(downloadUrl);
    if(this.isAddProductRequest) {
      this.addProductToFirebase(this.productId, downloadUrl)
    } else {
      this.updateProductData(this.productId, downloadUrl);
    }
  }

  onError(error: any) {
    console.log(error);
  }

}
