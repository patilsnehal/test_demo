import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as firebase from 'firebase/app';
import { FormGroup } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  selectedVal: string;
  public email_id:any;
  public password:any;

  constructor( private titleService: Title, 
    public angFirestore: AngularFirestore, 
    public afAuth: AngularFireAuth, 
    public router: Router,
    
    ) {
    this.selectedVal = 'login';
   }

  ngOnInit() {
    const appTitle = this.titleService.getTitle();
    this.titleService.setTitle('Login');
    setTimeout (() => {
      let intro =document.getElementById('intro');
      let login =document.getElementById('login');
      intro.style.display='none';
      login.style.display='block';
   }, 4000);
  }
  signin(event) {
    console.log("Event "+event.keyCode);
    firebase.auth().signInWithEmailAndPassword(this.email_id, this.password).then(newAccount=>{
      this.afAuth.authState.subscribe(user=>{
        if(user){
          let toast = document.getElementById('Successtoast');
          toast.style.display = 'block';
          this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).valueChanges().subscribe(val => {

            if (event != 'undefined' && event.keyCode === 13) {
              //event.preventDefault();
              if (val[0]['role'] === 'user') {
                this.router.navigate(['home']);
              } else if (val[0]['role'] === 'admin') {
                this.router.navigate(['admin']);
              }
            }
            else{
              if (val[0]['role'] === 'user') {
                this.router.navigate(['home']);
              } else if (val[0]['role'] === 'admin') {
                this.router.navigate(['admin']);
              }
            }
           
          });
          setTimeout(() => {
            toast.style.display = 'none';
           
            //this.router.navigate(['/home']);
          }, 2000);
        }
      }) 
    }).catch(err=>{
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
    });

    
  } 
  }