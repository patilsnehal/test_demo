import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';



@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.scss']
})
export class ForgotPassComponent implements OnInit {

  public email:any;

  constructor(
    public auth: AngularFireAuth , 
    public angFirestore: AngularFirestore,
    public router: Router ,
  ) { }
 
  ngOnInit() {
  }
  forgotpass(){
    if(this.email !== "") {
      firebase.auth().sendPasswordResetEmail(this.email).then(res => {
          console.log("Forgot password link sent succeessfully on your mail");
          let toast = document.getElementById('resetpass');
          toast.style.display = 'block';
          setTimeout(() => {
            toast.style.display = 'none';
           
          }, 2000); 
      }).catch(error => {
        console.log(error); 
        let toast = document.getElementById('wrongemail');
          toast.style.display = 'block';
          setTimeout(() => {
            toast.style.display = 'none';
           
          }, 2000); 
        
      });
    }else {
      console.log('Please Enter your email');
    }
  }

}
