import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {


  public item: any;

  constructor(public angFirestore: AngularFirestore) { }

  ngOnInit() {
    this.get_data();
  }

  get_data() {
    this.angFirestore.collection('newAccount').valueChanges().subscribe(val=>{
      this.item = val;
    });
  }

}
