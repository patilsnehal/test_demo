import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
//import { FirebaseListObservable } from './product-serch.service';

@Injectable({
  providedIn: 'root'
})
export class ProductSerchService {

  constructor(private db: AngularFireDatabase) { }

  // getProductSerch(start, end): FirebaseListObservable<any>{
  //   return this.db.list('/product', {
  //     query: {
  //       orderByChild: 'name',
  //       limitToFirst: 5,
  //       startAt: start,
  //       endAt: end
  //     }
  //   });
  // }
}
