import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-myordersproduct',
  templateUrl: './myordersproduct.component.html',
  styleUrls: ['./myordersproduct.component.scss']
})
export class MyordersproductComponent implements OnInit {

  orderId : any;
  today : any;

  public cart: any = [];
  public total_item: any;
  public total_qty = 0;
  public total: any;
  public users: any;
  public fname: any;
  public lname: any;
  public phone: any;
  public address: any;
  public email: any;
  public totalAmt = 0;
  public order_status = false;

  public brand: any;
  public measure: any;
  public name: any;
  public payment_status: any;
  public price: any;
  public product_quantity: any;
  public uid: any;

  unique_id:any;

  constructor(public angFirestore: AngularFirestore,
    public router: Router,
    public route: ActivatedRoute,
    public angularFireAuth: AngularFireAuth) { }

  ngOnInit() {
    this.get_product();
    this.route.queryParams.subscribe((res) => {
      this.unique_id = res['unique_id'];
    });
  }
  get_product() {
    this.cart = [];
    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('myOrders',ref=>ref.where('unique_id','==',this.unique_id)).valueChanges().subscribe(res=>{
        this.today = res[0]['timestamp'];
        this.orderId = res[0]['order_id'];
        this.order_status = res[0]['order_status'];
        console.log(res);
        this.angFirestore.collection('myOrderProducts',ref=>ref.where('user_id','==',user.uid)).valueChanges().subscribe(res1=>{
          console.log(res1);
          // res1.forEach(data=>{
          //   this.angFirestore.collection('myOrders').doc(data['unique_id']).delete().then(res=>{console.log('Delete All Order');});
          // });
        });
          // res.forEach(data=>{
          //   this.angFirestore.collection('myOrders').doc(data['unique_id']).delete().then(res=>{console.log('Delete All Order');});
          // });
        });
        
        this.angFirestore.collection('myOrderProducts',ref=>ref.where('order_id','==',this.unique_id)).valueChanges().subscribe(res1=>{
          let price = 0;
            this.total = 0;
            res1.forEach(data => {
              price = 0;
              price = Number(price) + (Number(data['price']) * Number(data['product_quantity']));
              this.total = Number(this.total) + (Number(data['price']) * Number(data['product_quantity']));
              Array.prototype.push.apply(data, [{ total: price }]);
              this.cart.push(data);
            });
            
          this.totalAmt = Number(this.total) + 40;
            this.total_item = res1.length;
            //this.total_qty = this.total_qty + (Number(res1['total_item'])); 
            console.log('total all items' + this.total_item);

            res1.forEach(
              data => {
                this.total_qty = this.total_qty + (Number(data['product_quantity']));
              }
            );

            console.log(res1);
            console.log('total all qty' + this.total_qty);
        });  

      }
    });

    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).
          valueChanges().subscribe(val => {
            this.users = val;
            console.log(val);
            this.fname = val[0]['firstName'];
            this.lname = val[0]['lastName'];
            this.phone = val[0]['mobileNumber'];
            this.address = val[0]['address'];
            console.log(this.address);
            this.email = val[0]['emailId'];
          });
      }
    });
  }

  order_receive(id){

    this.angFirestore.collection('myOrders').doc(id).update({order_status: true});

  }
}

