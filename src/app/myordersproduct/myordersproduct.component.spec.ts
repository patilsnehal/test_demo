import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyordersproductComponent } from './myordersproduct.component';

describe('MyordersproductComponent', () => {
  let component: MyordersproductComponent;
  let fixture: ComponentFixture<MyordersproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyordersproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyordersproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
