import { Component } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Subject } from 'rxjs';

import { FormControl } from '@angular/forms';
// import { Observable } from 'rxjs/Observable';
import { SAMPLE_RESULTS } from './SAMPLE_RESULTS';
// import { startWith, map } from 'rxjs/operators';
// import 'rxjs/add/operator/startWith';
// import 'rxjs/add/operator/map';

//import { ProductSerchService } from './product-serch.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'daily-needs';
  name: any;
  public role: any;

  public isLoginIn: boolean = false;
  email: any;
  address: any;
  profileImage: any; 
  productC: any = [];
  public product1: any;
  public product: any;

  productsearch;
  startAt =new Subject()
  endAt =new Subject()

  searchControl: FormControl;

  // filteredResults$: Observable<string[]>;

  results = SAMPLE_RESULTS;

  constructor(public router: Router, 
    public angularFireAuth: AngularFireAuth, 
    public angFirestore: AngularFirestore,
    
    //public searchproductsvc: ProductSerchService

) {

  this.searchControl = new FormControl('');
  // this.filteredResults$ = this.searchControl.valueChanges
  //   .startWith('')
  //   .map(val => this.filterResults(val))
  //   .map(val => val.slice(0, 4));
  } 

  ngOnInit() {
    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.isLoginIn = true;
        console.log(user);
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).valueChanges().subscribe(val => {
          console.log(val);
          this.name = val[0]['firstName'];
          this.email = val[0]['emailId'];
          this.address = val[0]['address'];
          this.profileImage = val[0]['profileImage'];
          this.role = val[0]['role'];
        });
      } else {
        this.isLoginIn = false;
        this.router.navigate(['/login']);
      }
    })

    this.angFirestore.collection('Product_Category').valueChanges().subscribe(res => {
      res.forEach((data, index) => {
        this.angFirestore.collection('Product_Category').doc(data['unique_id'])
        .collection('HomeSlider').valueChanges().subscribe(slide => {
          console.log(slide);
          Array.prototype.push.apply(data, [{ slide }]);
          this.productC.push(data);
          console.log("ProductC :" + this.productC);
        });
      });
    });

    this.angFirestore.collection('SubProductCategory', ref => ref.where('IsActive', '==', 1).orderBy('name', 'asc')).
    valueChanges().subscribe(val => {
      this.product1 = val;
      this.product = this.product1;
      console.log(val);
    });


    // this.searchproductsvc.getProductSerch(this.startAt, this.endAt)
    // .subscribe(productsearch => this.productsearch = productsearch)


  }

  //  search($event){
  //    let q = $event.target.value
  //    this.startAt.next(q)
  //    this.endAt.next(q+"\uf8ff")
  //  }

  logout() {
    this.angularFireAuth.auth.signOut();
    this.router.navigate(['/login']); 
  }
  viewProduct(id: any, name: any) {
    this.router.navigate(['/productlist'], {
      queryParams: { unique_id: id, section: name },
    });
  }

  initializeItems(): void {
    this.product1 = this.product;
  }

    //Important Serach Bar
    getItems(ev: any) {
      this.initializeItems();
      const val = ev.target.value;
      if (!val) {
        return;
      }
      this.product1 = this.product1.filter(currentGoal => {
        if (currentGoal.name && val) {
          if (currentGoal.name.indexOf(val) > -1) {
            return true;
          }
          return false;
        }
      });
    }
    private filterResults(val: string): string[] {
      return val ? this.results.filter(v => v.toLowerCase().indexOf(val.toLowerCase()) === 0) : [];
    }
}
