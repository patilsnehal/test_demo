import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
 
  public item: any;
  public product_name: any;
  public uid: any;
  public unique_id: any;

  constructor(public angFirestore: AngularFirestore, public router: Router, public afauth:AngularFireAuth) { }

  ngOnInit() {
    this.get_product();
  }
  get_product() {
    this.angFirestore.collection('Product_Category').valueChanges().subscribe(res => {
      this.item = res;
      console.log(res);
    });
  }

  addproduct() {
    const newId = this.angFirestore.createId();
    this.angFirestore.collection('Product_Category').doc(newId).set({
      name: this.product_name,
      unique_id: newId
    }).then(res => {
      this.get_product();
    });
  }

  update(id) { 
    this.angFirestore.collection('Product_Category').doc(id).update({
      name: this.product_name
    })
  }
  edit(id) {
    this.angFirestore.collection('Product_Category', ref => ref.where('unique_id', '==', id))
      .valueChanges().subscribe(res => {
        this.product_name = res[0]['name'],
        this.uid = res[0]['unique_id']
      })
  }

  viewProduct(item: any) {
    this.router.navigate(['/viewproductcategory'], {
      queryParams: { unique_id: item["unique_id"], section: item["name"] },
    });
  }

  deleteproduct(id) {
    this.unique_id = id;
  }
  delete(id) {
    this.angFirestore.collection('Product_Category').doc(id).delete();
  }
  logout(){
    this.afauth.auth.signOut();
    this.router.navigate(['/login']);
  }
}
