import { AngularFireStorage } from 'angularfire2/storage';
import { finalize } from 'rxjs/operators';
import { ImageUploadListener } from './ImageUploadListener';

export class ImageUploadManager {

    public static uploadImageToFirebase(angularFireStorage: AngularFireStorage,
        localFilePath: any,
        firebaseFilePath: string,
        imageUploadLisrener: ImageUploadListener) {
        console.log("Image upload started for path :" + firebaseFilePath);
        const fileRef = angularFireStorage.ref(firebaseFilePath);
        const task = angularFireStorage.upload(firebaseFilePath, localFilePath);
        task.snapshotChanges()
            .pipe(finalize(() => fileRef.getDownloadURL().subscribe(downloadUrl => {
                if (downloadUrl) {
                    console.log("Imagge upload success :" + downloadUrl);
                    imageUploadLisrener.onSuccess(downloadUrl)
                }
            }, error => {
                console.log("Image upload error");
                imageUploadLisrener.onError(error)
            }))).subscribe(uloadSnapshot => {
                console.log(uloadSnapshot.bytesTransferred);
            });
    }
}