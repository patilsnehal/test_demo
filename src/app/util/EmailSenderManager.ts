import { HttpClient } from '@angular/common/http';

export class EmailSenderManager {

    public static sendOrderConfirmationEmail(httpClient: HttpClient,
        emailBody: any) {
        console.log("emailBody : " + emailBody)
        httpClient.post("http://localhost:3000/superDailyNeed/sendOrderConfirmationEmail", emailBody)
            .subscribe({
                next: data => console.log(data),
                error: error => console.log(error)
            });
    }
}