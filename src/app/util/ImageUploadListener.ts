export interface ImageUploadListener {
    
    onSuccess(downloadUrl: string) : any;

    onError(error: any) : any;
}