import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-customer-support',
  templateUrl: './customer-support.component.html',
  styleUrls: ['./customer-support.component.scss']
})
export class CustomerSupportComponent implements OnInit {


  public message: any;
  public role: any;
  public firstName: any;
  public lastName: any;
  public emailId: any;
  public item: any;

  constructor(public  angFirestore: AngularFirestore, public auth:AngularFireAuth) { }

  ngOnInit() {

    this.auth.authState.subscribe(user =>{
      if(user) {
        this.angFirestore.collection('newAccount',ref=> ref.where('emailId', '==', user.email)).valueChanges().subscribe(val=>{
          console.log(val);
          this.role = val[0]['role'];
          this.firstName = val[0]['firstName'];
          this.lastName = val[0]['lastName'];
          this.emailId = val[0]['emailId'];
        });
      }
    });

    this.get_data();

  }

  get_data() {
    this.angFirestore.collection('contact_us').valueChanges().subscribe(val=>{
      this.item = val;
    });
  }

  contact_us() {
    const newId = this.angFirestore.createId();
    this.angFirestore.collection('contact_us').doc(newId).set({
      message: this.message,
      unique_id: newId,
      firstName: this.firstName,
      lastName: this.lastName,
      emailId: this.emailId,
      status: 'unread'
    });
  }

  status(id){
    this.angFirestore.collection('contact_us').doc(id).update({status: 'read'});
  }

}
