import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  public item: any;
  order_id: any;
  public brand: any;
  public measure: any;
  public name: any;
  public image: any;
  //public payment_status: any;
  public price: any;
  public product_quantity: any;
  public uid: any;

  public cart = [];


  constructor(public angFirestore: AngularFirestore, public router: Router, public angularFireAuth: AngularFireAuth) { }

  ngOnInit() {
    this.get_product();
  }

  get_product() {
    this.item = [];
    this.angFirestore.collection('myOrders').valueChanges().subscribe(res => {
      res.forEach(data => {
        this.angFirestore.collection('newAccount', ref => ref.where('user_uid', '==', data['user_uid']))
        .valueChanges().subscribe(user => {
          Array.prototype.push.apply(data, [{ name: user[0]['firstName'] + ' ' + user[0]['lastName'], email: user[0]['emailId'] }]);
          this.item.push(data);
        });
      });
      console.log(this.item);
    });
  }
  getOrderProduct(id) {
    this.cart = [];
    this.angFirestore.collection('myOrderProducts', ref => ref.where('order_id', '==', id)).
          valueChanges().subscribe(val =>{
            this.cart = val       
    });
  }

}
