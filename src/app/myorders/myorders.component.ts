import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.component.html',
  styleUrls: ['./myorders.component.scss']
})
export class MyordersComponent implements OnInit {

  orderId : any;
  today : any;

  public orders: any = [];
  public total_item: any;

  constructor(public angFirestore: AngularFirestore,
    public router: Router,
    public route: ActivatedRoute,
    public angularFireAuth: AngularFireAuth) { }

  ngOnInit() {
    this.today = formatDate(new Date(),'dd MMM yyyy', 'en-US', '+0530');
    this.orderId = Math.floor((Math.random() * 1000000000) + 100000000);
    this.get_product();
  }
  get_product() {
    this.orders = [];
    this.angularFireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.angFirestore.collection('myOrders',ref=>ref.where('user_uid','==',user.uid)).valueChanges().subscribe(res=>{
          console.log('length : '+ res.length);
          console.log(res);
          this.orders = res;
          this.total_item = res.length;
          this.angFirestore.collection('myOrderProducts',ref=>ref.where('user_uid','==',user.uid)).valueChanges().subscribe(res1=>{
            console.log('lengthProduct : '+ res1.length);
            console.log(res1);  
            // res1.forEach(data=>{
            //   this.angFirestore.collection('myOrderProducts').doc(data['unique_id']).delete().then(res=>{console.log('Delete All Order');});
            // });
          });  
          // res.forEach(data=>{
          //   this.angFirestore.collection('myOrders').doc(data['unique_id']).delete().then(res=>{console.log('Delete All Order');});
          // });
        });

      }
    });
  }

  getOrderProduct(id){
    this.router.navigate(['/myordersproduct'], {
      queryParams: {unique_id:id},
    });
  }

}
