import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveredorderComponent } from './deliveredorder.component';

describe('DeliveredorderComponent', () => {
  let component: DeliveredorderComponent;
  let fixture: ComponentFixture<DeliveredorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveredorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveredorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
