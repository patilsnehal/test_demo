import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from 'angularfire2/storage';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public items: any;
  public unique_id:any;
  public product_unique_id: any;
  public product_quantity = 1;

  public image:any;
  public brand:any;
  public name:any;
  public measure:any;
  public price:any;
public uid:any;
  constructor( 
    public auth: AngularFireAuth,
    public angfstorage: AngularFireStorage,
    public route: ActivatedRoute,
    public angFirestore: AngularFirestore,
    public router: Router
  ) { }

  ngOnInit() {
    this.auth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
    });
    this.route.queryParams.subscribe((res) => {
      this.unique_id = res['unique_id'];
      this.product_unique_id = res['product_unique_id'];
      console.log("Data receive of uniqueid" + this.unique_id);
      console.log("product_unique_id : " + this.product_unique_id);
    });
    this.get_product();

  } 

  get_product() {
    console.log("uniqueid" + this.unique_id);
    this.angFirestore.collection('Product_Category').doc(this.product_unique_id).
      collection('ProductCategory').doc(this.unique_id).
      collection('SubProductCategory').valueChanges().subscribe(res => {
        this.items = res;
        console.log(res);
      });
  }
 
  
  addtocart(id,image,name,price,measure,brand){
    console.log(this.uid);
    this.auth.authState.subscribe(users=>{
      if(users){
        this.angFirestore.collection('cart',ref=>ref.where('name','==',name).where('uid','==',users.uid)).get().subscribe(res=>{
          console.log(res.size);
          if(res.size === 1){
            let toast = document.getElementById('productalready');
            toast.style.display = 'block';
            setTimeout(() => {
              toast.style.display = 'none';
            }, 2000);
          }else{
             
            const newId = this.angFirestore.createId();
        this.angFirestore.collection('cart').doc(newId).set({
          brand: brand,
          unique_id: newId,
          name: name,
          image: image,
          product_id:id,
          measure: measure,
          price: price,
          Product_Category:this.product_unique_id,
          ProductCategory : this.unique_id,
          payment_status : false,
          product_quantity : this.product_quantity,
          uid:users.uid
        
        }).then(res=>{
          let toast = document.getElementById('productadd');
            toast.style.display = 'block';
            setTimeout(() => {
              toast.style.display = 'none';
            }, 2000);
        });
          }
        });
      }
    })
  }

}
  