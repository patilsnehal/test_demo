import { Component, OnInit, ɵConsole, Output, EventEmitter } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { from } from 'rxjs';
import $ from "jquery";
//import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
}) 
export class HomeComponent implements OnInit {

  //public slide1: any;
  name: any;
  email: any;
  address: any;
  productC: any = [];

  constructor(public auth: AngularFireAuth,
    public angFirestore: AngularFirestore,
    public router: Router,
  ) {
  }

  ngOnInit() {

    window.setInterval(function(){
      setTimeout(() => {
        document.getElementById('nextBtn1').click();
        document.getElementById('nextBtn2').click();
        document.getElementById('nextBtn3').click();
        document.getElementById('nextBtn4').click();
      }, 5000); 
    }, 5000);
    
    this.auth.authState.subscribe(user => {
      if (user) {
        console.log(user);
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).valueChanges().subscribe(val => {
          console.log(val);
          this.name = val[0]['firstName'];
          this.email = val[0]['emailId'];
          this.address = val[0]['address'];
        });
      }
    });

    this.angFirestore.collection('Product_Category').valueChanges().subscribe(res => {
      res.forEach((data, index) => {
        this.angFirestore.collection('Product_Category').doc(data['unique_id']).collection('HomeSlider').valueChanges().subscribe(slide => {
          console.log(slide);
          Array.prototype.push.apply(data, [{ slide }]);
          this.productC.push(data);
          console.log("ProductC :" + this.productC);
        });
      });
    });
  }

  viewProduct(id: any, name: any) {
    this.router.navigate(['/productlist'], {
      queryParams: { unique_id: id, section: name },
    });
  }
}


