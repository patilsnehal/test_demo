import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from 'angularfire2/storage';
import { map, finalize } from 'rxjs/operators'
import { from } from 'rxjs';
import { ImageUploadManager } from '../util/ImageUploadManager';
import { ImageUploadListener } from '../util/ImageUploadListener';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, ImageUploadListener {

  uid: any;
  name: any;
  lname: any;
  email: any;
  mobile: any;
  bdate: any;
  line1: any;
  line2: any;
  pincode: any;
  city: any;
  file: any;
  profileImage: any;
  constructor(public auth: AngularFireAuth,
    public angFirestore: AngularFirestore,
    public router: Router,
    public angfstorage: AngularFireStorage
  ) { }
  onSuccess(downloadUrl: string) {
    console.log(downloadUrl);
    this.updateUserData(downloadUrl);
  }
  onError(error: any) {
    console.log(error);
  }
  ngOnInit() {
    this.auth.authState.subscribe(user => {
      if (user) {
        this.angFirestore.collection('newAccount', ref => ref.where('emailId', '==', user.email)).valueChanges().subscribe(val => {
          console.log(val);
          this.name = val[0]['firstName'];
          this.lname = val[0]['lastName'];
          this.email = val[0]['emailId'];
          this.mobile = val[0]['mobileNumber'];
          this.bdate = val[0]['dob'];
          this.line1 = val[0]['address']['Line_1'];
          this.line2 = val[0]['address']['Line_2'];
          this.pincode = val[0]['address']['Pincode'];
          this.city = val[0]['address']['City'];
          this.uid = val[0]['uid'];
          this.profileImage = val[0]['profileImage'];
          console.log(this.profileImage);
        });
      }
    })
  }
  save() {
    if (this.file) {
      console.log(this.file);
      ImageUploadManager.uploadImageToFirebase(this.angfstorage,
        this.file,
        "ProfileImages/"+this.email,
        this);
   } else {
     this.updateUserData(null);
   }

  }

  editimage(event) {
    this.file = event.target.files[0];
    console.log(this.file);
  }

  private updateUserData(downloadUrl: string) {
    this.angFirestore.collection('newAccount').doc(this.uid).update({
      firstName: this.name,
      lastName: this.lname,
      mobileNumber: this.mobile,
      dob: this.bdate,
      address: { City: this.city, Line_1: this.line1, Line_2: this.line2, Pincode: this.pincode },
      profileImage: downloadUrl
    }).then(value => {
      console.log("Updated Successful.")
    },
      reason => {
        console.log("Update failed " + reason);
      });
  }
 } 