(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <app-layout-header></app-layout-header> -->\r\n<router-outlet></router-outlet>\r\n<!-- <app-layout-footer></app-layout-footer> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>cart works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/creat-account/creat-account.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/creat-account/creat-account.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>creat-account works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/customer-support/customer-support.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/customer-support/customer-support.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>customer-support works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-layout-header></app-layout-header>\r\n<style>\r\n  @media (min-width: 768px) {\r\n    .main-content {\r\n      margin-left: 250px;\r\n    }\r\n  }\r\n</style>\r\n<div class=\"main-content\">\r\n  <app-layout-topmenu></app-layout-topmenu>\r\n  <!-- Header -->\r\n  <div class=\"header bg-gradient-primary pb-8 pt-5 pt-md-8\">\r\n    <div class=\"container-fluid\">\r\n      <div class=\"header-body\">\r\n        <!-- Card stats -->\r\n        <div class=\"row\">\r\n          <div class=\"col-xl-3 col-lg-6\">\r\n            <div class=\"card card-stats mb-4 mb-xl-0\">\r\n              <div class=\"card-body\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <h5 class=\"card-title text-uppercase text-muted mb-0\">Traffic</h5>\r\n                    <span class=\"h2 font-weight-bold mb-0\">350,897</span>\r\n                  </div>\r\n                  <div class=\"col-auto\">\r\n                    <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n                      <i class=\"fas fa-chart-bar\"></i>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                  <span class=\"text-success mr-2\"><i class=\"fa fa-arrow-up\"></i> 3.48%</span>\r\n                  <span class=\"text-nowrap\">Since last month</span>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-xl-3 col-lg-6\">\r\n            <div class=\"card card-stats mb-4 mb-xl-0\">\r\n              <div class=\"card-body\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <h5 class=\"card-title text-uppercase text-muted mb-0\">New users</h5>\r\n                    <span class=\"h2 font-weight-bold mb-0\">2,356</span>\r\n                  </div>\r\n                  <div class=\"col-auto\">\r\n                    <div class=\"icon icon-shape bg-warning text-white rounded-circle shadow\">\r\n                      <i class=\"fas fa-chart-pie\"></i>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                  <span class=\"text-danger mr-2\"><i class=\"fas fa-arrow-down\"></i> 3.48%</span>\r\n                  <span class=\"text-nowrap\">Since last week</span>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-xl-3 col-lg-6\">\r\n            <div class=\"card card-stats mb-4 mb-xl-0\">\r\n              <div class=\"card-body\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <h5 class=\"card-title text-uppercase text-muted mb-0\">Sales</h5>\r\n                    <span class=\"h2 font-weight-bold mb-0\">924</span>\r\n                  </div>\r\n                  <div class=\"col-auto\">\r\n                    <div class=\"icon icon-shape bg-yellow text-white rounded-circle shadow\">\r\n                      <i class=\"fas fa-users\"></i>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                  <span class=\"text-warning mr-2\"><i class=\"fas fa-arrow-down\"></i> 1.10%</span>\r\n                  <span class=\"text-nowrap\">Since yesterday</span>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-xl-3 col-lg-6\">\r\n            <div class=\"card card-stats mb-4 mb-xl-0\">\r\n              <div class=\"card-body\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <h5 class=\"card-title text-uppercase text-muted mb-0\">Performance</h5>\r\n                    <span class=\"h2 font-weight-bold mb-0\">49,65%</span>\r\n                  </div>\r\n                  <div class=\"col-auto\">\r\n                    <div class=\"icon icon-shape bg-info text-white rounded-circle shadow\">\r\n                      <i class=\"fas fa-percent\"></i>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                  <span class=\"text-success mr-2\"><i class=\"fas fa-arrow-up\"></i> 12%</span>\r\n                  <span class=\"text-nowrap\">Since last month</span>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"container-fluid mt--7\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-8 mb-5 mb-xl-0\">\r\n        <div class=\"card bg-gradient-default shadow\">\r\n          <div class=\"card-header bg-transparent\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col\">\r\n                <h6 class=\"text-uppercase text-light ls-1 mb-1\">Overview</h6>\r\n                <h2 class=\"text-white mb-0\">Sales value</h2>\r\n              </div>\r\n              <div class=\"col\">\r\n                <ul class=\"nav nav-pills justify-content-end\">\r\n                  <li class=\"nav-item mr-2 mr-md-0\" data-toggle=\"chart\" data-target=\"#chart-sales\"\r\n                    data-update='{\"data\":{\"datasets\":[{\"data\":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix=\"$\"\r\n                    data-suffix=\"k\">\r\n                    <a href=\"#\" class=\"nav-link py-2 px-3 active\" data-toggle=\"tab\">\r\n                      <span class=\"d-none d-md-block\">Month</span>\r\n                      <span class=\"d-md-none\">M</span>\r\n                    </a>\r\n                  </li>\r\n                  <li class=\"nav-item\" data-toggle=\"chart\" data-target=\"#chart-sales\"\r\n                    data-update='{\"data\":{\"datasets\":[{\"data\":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix=\"$\"\r\n                    data-suffix=\"k\">\r\n                    <a href=\"#\" class=\"nav-link py-2 px-3\" data-toggle=\"tab\">\r\n                      <span class=\"d-none d-md-block\">Week</span>\r\n                      <span class=\"d-md-none\">W</span>\r\n                    </a>\r\n                  </li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <!-- Chart -->\r\n            <div class=\"chart\">\r\n              <!-- Chart wrapper -->\r\n              <canvas id=\"chart-sales\" class=\"chart-canvas\"></canvas>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4\">\r\n        <div class=\"card shadow\">\r\n          <div class=\"card-header bg-transparent\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col\">\r\n                <h6 class=\"text-uppercase text-muted ls-1 mb-1\">Performance</h6>\r\n                <h2 class=\"mb-0\">Total orders</h2>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <!-- Chart -->\r\n            <div class=\"chart\">\r\n              <canvas id=\"chart-orders\" class=\"chart-canvas\"></canvas>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row mt-5\">\r\n      <div class=\"col-xl-8 mb-5 mb-xl-0\">\r\n        <div class=\"card shadow\">\r\n          <div class=\"card-header border-0\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col\">\r\n                <h3 class=\"mb-0\">Page visits</h3>\r\n              </div>\r\n              <div class=\"col text-right\">\r\n                <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"table-responsive\">\r\n            <!-- Projects table -->\r\n            <table class=\"table align-items-center table-flush\">\r\n              <thead class=\"thead-light\">\r\n                <tr>\r\n                  <th scope=\"col\">Page name</th>\r\n                  <th scope=\"col\">Visitors</th>\r\n                  <th scope=\"col\">Unique users</th>\r\n                  <th scope=\"col\">Bounce rate</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    /argon/\r\n                  </th>\r\n                  <td>\r\n                    4,569\r\n                  </td>\r\n                  <td>\r\n                    340\r\n                  </td>\r\n                  <td>\r\n                    <i class=\"fas fa-arrow-up text-success mr-3\"></i> 46,53%\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    /argon/index.html\r\n                  </th>\r\n                  <td>\r\n                    3,985\r\n                  </td>\r\n                  <td>\r\n                    319\r\n                  </td>\r\n                  <td>\r\n                    <i class=\"fas fa-arrow-down text-warning mr-3\"></i> 46,53%\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    /argon/charts.html\r\n                  </th>\r\n                  <td>\r\n                    3,513\r\n                  </td>\r\n                  <td>\r\n                    294\r\n                  </td>\r\n                  <td>\r\n                    <i class=\"fas fa-arrow-down text-warning mr-3\"></i> 36,49%\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    /argon/tables.html\r\n                  </th>\r\n                  <td>\r\n                    2,050\r\n                  </td>\r\n                  <td>\r\n                    147\r\n                  </td>\r\n                  <td>\r\n                    <i class=\"fas fa-arrow-up text-success mr-3\"></i> 50,87%\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    /argon/profile.html\r\n                  </th>\r\n                  <td>\r\n                    1,795\r\n                  </td>\r\n                  <td>\r\n                    190\r\n                  </td>\r\n                  <td>\r\n                    <i class=\"fas fa-arrow-down text-danger mr-3\"></i> 46,53%\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4\">\r\n        <div class=\"card shadow\">\r\n          <div class=\"card-header border-0\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col\">\r\n                <h3 class=\"mb-0\">Social traffic</h3>\r\n              </div>\r\n              <div class=\"col text-right\">\r\n                <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"table-responsive\">\r\n            <!-- Projects table -->\r\n            <table class=\"table align-items-center table-flush\">\r\n              <thead class=\"thead-light\">\r\n                <tr>\r\n                  <th scope=\"col\">Referral</th>\r\n                  <th scope=\"col\">Visitors</th>\r\n                  <th scope=\"col\"></th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    Facebook\r\n                  </th>\r\n                  <td>\r\n                    1,480\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-flex align-items-center\">\r\n                      <span class=\"mr-2\">60%</span>\r\n                      <div>\r\n                        <div class=\"progress\">\r\n                          <div class=\"progress-bar bg-gradient-danger\" role=\"progressbar\" aria-valuenow=\"60\"\r\n                            aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    Facebook\r\n                  </th>\r\n                  <td>\r\n                    5,480\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-flex align-items-center\">\r\n                      <span class=\"mr-2\">70%</span>\r\n                      <div>\r\n                        <div class=\"progress\">\r\n                          <div class=\"progress-bar bg-gradient-success\" role=\"progressbar\" aria-valuenow=\"70\"\r\n                            aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%;\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    Google\r\n                  </th>\r\n                  <td>\r\n                    4,807\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-flex align-items-center\">\r\n                      <span class=\"mr-2\">80%</span>\r\n                      <div>\r\n                        <div class=\"progress\">\r\n                          <div class=\"progress-bar bg-gradient-primary\" role=\"progressbar\" aria-valuenow=\"80\"\r\n                            aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%;\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    Instagram\r\n                  </th>\r\n                  <td>\r\n                    3,678\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-flex align-items-center\">\r\n                      <span class=\"mr-2\">75%</span>\r\n                      <div>\r\n                        <div class=\"progress\">\r\n                          <div class=\"progress-bar bg-gradient-info\" role=\"progressbar\" aria-valuenow=\"75\"\r\n                            aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 75%;\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <th scope=\"row\">\r\n                    twitter\r\n                  </th>\r\n                  <td>\r\n                    2,645\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-flex align-items-center\">\r\n                      <span class=\"mr-2\">30%</span>\r\n                      <div>\r\n                        <div class=\"progress\">\r\n                          <div class=\"progress-bar bg-gradient-warning\" role=\"progressbar\" aria-valuenow=\"30\"\r\n                            aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 30%;\"></div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <app-layout-footer></app-layout-footer>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-pass/forgot-pass.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-pass/forgot-pass.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>forgot-pass works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/garbadge-disposal/garbadge-disposal.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/garbadge-disposal/garbadge-disposal.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>garbadge-disposal works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- Navigation Bar list start -->\n<nav class=\"navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\n    <div class=\"container-fluid\">\n      <!-- Toggler -->\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidenav-collapse-main\"\n        aria-controls=\"sidenav-main\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n      </button>\n      <!-- Brand Name in navigation-->\n      <a class=\"navbar-brand pt-0\" href=\"/\">\n        <h2 style=\"color: #93291e;\">Super Daily Needs</h2>\n        <img src=\"assets/img/shopping-cart.svg\" alt=\"logo\"  class=\"animated zoomIn\">\n      </a>\n      <!-- User click -->\n      <ul class=\"nav align-items-center d-md-none\">\n        <li class=\"nav-item dropdown\">\n          <a class=\"nav-link nav-link-icon\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n            aria-expanded=\"false\">\n            <i class=\"ni ni-bell-55\"></i>\n          </a>\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\" aria-labelledby=\"navbar-default_dropdown_1\">\n            <a class=\"dropdown-item\" href=\"#\">Action</a>\n            <a class=\"dropdown-item\" href=\"#\">Another action</a>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n          </div>\n        </li>\n        <li class=\"nav-item dropdown\">\n          <a class=\"nav-link\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            <div class=\"media align-items-center\">\n              <span class=\"avatar avatar-sm rounded-circle\">\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\n              </span>\n            </div>\n          </a>\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\">\n            <div class=\" dropdown-header noti-title\">\n              <h6 class=\"text-overflow m-0\">Welcome To Super Daily Needs</h6>\n            </div>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-single-02\"></i>\n              <span>My profile</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-settings-gear-65\"></i>\n              <span>Settings</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-calendar-grid-58\"></i>\n              <span>Activity</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-support-16\"></i>\n              <span>Support</span>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a href=\"#!\" class=\"dropdown-item\">\n              <i class=\"ni ni-button-power\"></i>\n              <span>Logout</span>\n            </a>\n          </div>\n        </li>\n      </ul>\n      <!-- Collapse -->\n      <div class=\"collapse navbar-collapse\" id=\"sidenav-collapse-main\">\n        <!-- Collapse header -->\n        <div class=\"navbar-collapse-header d-md-none\">\n          <div class=\"row\">\n            <div class=\"col-6 collapse-brand\">\n              <a href=\"./index.html\">\n                <img src=\"./assets/img/brand/blue.png\">\n              </a>\n            </div>\n            <div class=\"col-6 collapse-close\">\n              <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#sidenav-collapse-main\"\n                aria-controls=\"sidenav-main\" aria-expanded=\"false\" aria-label=\"Toggle sidenav\">\n                <span></span>\n                <span></span>\n              </button>\n            </div>\n          </div>\n        </div>\n        <!-- Navigation Bar list start -->\n        <ul class=\"navbar-nav\" >\n          <li class=\"nav-item  class=\" active>\n            <a class=\" nav-link\" [routerLink]=\"['/morningneeds']\">\n              <i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i><span style=\"color:#93291e;\">Morning Needs</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/vegfruits']\">\n              <i><img src=\"./assets/img/fruit.svg\" style=\"height: 20px;width: 20px;\"></i><span style=\"color:#93291e;\">Vegitables and Fruits</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/nonveg']\">\n              <i><img src=\"./assets/img/nonveg.svg\" style=\"height: 20px;width: 20px;\"></i><span style=\"color:#93291e;\">Non-veg</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/garbadgedisposal']\">\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i><span style=\"color:#93291e;\">Garbadge Disposal</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/offers']\">\n              <i class=\"fa fa-percent\"  aria-hidden=\"true\"></i><span style=\"color:#93291e;\">Offers</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/customersupport']\">\n              <i class=\"ni ni-circle-08\"></i><span style=\"color:#93291e;\"> Customer Support</span>\n            </a>\n          </li>\n          <li class=\"nav-item\">\n            <a class=\"nav-link \" [routerLink]=\"['/login']\">\n              <i class=\"fa fa-power-off\"></i><span style=\"color:#93291e;\"> Logout</span>\n            </a>\n          </li>\n        </ul>\n        <!-- Navigation Bar list end -->\n      </div>\n    </div>\n</nav> \n  <!-- Navigation Bar end -->\n<style>\n  @media (min-width: 768px) {\n    .main-content {\n      margin-left: 250px;\n    }\n  }\n</style>\n<div class=\"main-content\" >\n  <!--Top Menu start-->\n  <nav class=\"navbar navbar-top navbar-expand-md navbar-dark\" id=\"navbar-main\">\n    <div class=\"container-fluid\">\n      <!-- Form -->\n      <form class=\"navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto\">\n        <div class=\"form-group mb-0\">\n          <div class=\"input-group input-group-alternative\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\"><i class=\"fas fa-search\"></i></span>\n            </div>\n            <input class=\"form-control\" placeholder=\"Search\" type=\"text\">\n          </div>\n        </div>\n      </form>\n      <a [routerLink]=\"['/cart']\">\n        <i class=\"fa fa-shopping-cart\" style=\"font-size: xx-large;color:white;\" aria-hidden=\"true\"></i>\n      </a>\n      <!-- User -->\n      <ul class=\"navbar-nav align-items-center d-none d-md-flex\">\n        <li class=\"nav-item dropdown\">\n          <a class=\"nav-link pr-0\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            <div class=\"media align-items-center\">\n              <span class=\"avatar avatar-sm rounded-circle\">\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-4-800x800.jpg\">\n              </span>\n              <div class=\"media-body ml-2 d-none d-lg-block\">\n                <span class=\"mb-0 text-sm  font-weight-bold\"></span>\n              </div>\n            </div>\n          </a>\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\">\n            <div class=\" dropdown-header noti-title\">\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\n            </div>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-single-02\"></i>\n              <span>My profile</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-settings-gear-65\"></i>\n              <span>Settings</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-calendar-grid-58\"></i>\n              <span>Activity</span>\n            </a>\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\n              <i class=\"ni ni-support-16\"></i>\n              <span>Support</span>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a href=\"#!\" class=\"dropdown-item\">\n              <i class=\"ni ni-button-power\"></i>\n              <span>Logout</span>\n            </a>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </nav>\n  <!--Top Menu end-->\n  <!-- Header -->\n  <div class=\"header pb-8 pt-5 pt-md-8\" style=\"background: linear-gradient(to right, #ed213a, #93291e);\">\n    <div class=\"container-fluid\">\n      <div class=\"header-body\">\n        <h1 style=\"color: white;text-align: center;\">Super Daily Needs Products</h1>\n      </div>\n    </div>\n  </div>\n  <div class=\"container-fluid mt--7\" style=\"background: linear-gradient(to right, #ed213a, #93291e);\">\n    <!--Advertising slider 1-->\n    <div class=\"row mt-5\">\n      <div class=\"col-xl-6 mb-5 mb-xl-0\">\n        <div class=\"card shadow\">\n          <div class=\"card-header border-0\">\n            <div class=\"row align-items-center\">\n              <div class=\"col\">\n                <h3 class=\"mb-0\" style=\"color:#ed213a;\">Morning Needs</h3>\n              </div>\n              <div class=\"col text-right\">\n                <a href=\"#!\" class=\"btn btn-sm btn-success\">See all</a>\n              </div>\n            </div>\n          </div>\n          <!--Morning Needs Advertizing slider start-->\n          <div id=\"carouselExampleSlidesOnly\" class=\"carousel slide\" data-ride=\"carousel\">\n            <a href=\"#\">\n            <div class=\"carousel-inner\">\n              <div class=\"carousel-item active\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_milk.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Milk\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_bread.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Second slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_tea.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_juice.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_spread.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_maggi.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n            </div>\n          </a>\n          </div>\n          <!--Morning Needs Advertizing slider end-->\n        </div>\n      </div>\n      <div class=\"col-xl-6\">\n        <div class=\"card shadow\">\n          <div class=\"card-header border-0\">\n            <div class=\"row align-items-center\">\n              <div class=\"col\">\n                <h3 class=\"mb-0\" style=\"color:#ed213a;\">Vegitables & Fruits</h3>\n              </div>\n              <div class=\"col text-right\">\n                <a href=\"#!\" class=\"btn btn-sm btn-success\">See all</a>\n              </div>\n            </div>\n          </div>\n          <!--Vegitables & Fruits Advertizing slider start-->\n          <div id=\"carouselExampleSlidesOnly\" class=\"carousel slide\" data-ride=\"carousel\">\n            <a href=\"#\">\n            <div class=\"carousel-inner\">\n              <div class=\"carousel-item active\">\n                <div class=\"card img-fluid\" >\n                  <img class=\"card-img-top\" src=\"./assets/img/advrt_fruit1.jpg\" alt=\"Card image\" style=\"height: 300px;width: 500px;\">\n                  <div class=\"card-img-overlay\">\n                    <p class=\"card-text\" style=\"color: white;font-size: 30px;\">Natural Immunity Boosters</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_vegitable1.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Second slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_fruit2.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_vegitable2.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_fruit3.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n            </div>\n          </a>\n          </div>\n          <!--Vegitables & Fruits Advertizing slider end-->\n        </div>\n      </div>\n    </div>\n    <!--Advertising slider 2-->\n    <div class=\"row mt-5\" style=\"padding-bottom: 10px;\">\n      <div class=\"col-xl-6 mb-5 mb-xl-0\">\n        <div class=\"card shadow\">\n          <div class=\"card-header border-0\">\n            <div class=\"row align-items-center\">\n              <div class=\"col\">\n                <h3 class=\"mb-0\" style=\"color:#ed213a;\">Non-Veg</h3>\n              </div>\n              <div class=\"col text-right\">\n                <a href=\"#!\" class=\"btn btn-sm btn-success\">See all</a>\n              </div>\n            </div>\n          </div>\n          <!--Non-Veg Advertizing slider start-->\n          <div id=\"carouselExampleSlidesOnly\" class=\"carousel slide\" data-ride=\"carousel\">\n          <a href=\"#\">\n            <div class=\"carousel-inner\">\n              <div class=\"carousel-item active\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_egg.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Milk\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_fish.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Second slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_meat.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_mutton.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n            </div>\n          </a>\n          </div>\n          <!--Non-Veg Advertizing slider end-->\n        </div>\n      </div>\n      <div class=\"col-xl-6\">\n        <div class=\"card shadow\">\n          <div class=\"card-header border-0\">\n            <div class=\"row align-items-center\">\n              <div class=\"col\">\n                <h3 class=\"mb-0\" style=\"color:#ed213a;\">Garbadge Disposal</h3>\n              </div>\n              <div class=\"col text-right\">\n                <a href=\"#!\" class=\"btn btn-sm btn-success\">See all</a>\n              </div>\n            </div>\n          </div>\n          <!--Garbadge Disposal Advertizing slider start-->\n          <div id=\"carouselExampleSlidesOnly\" class=\"carousel slide\" data-ride=\"carousel\">\n            <a href=\"#\">\n            <div class=\"carousel-inner\">\n              <div class=\"carousel-item active\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_garbadge1.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Milk\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_garbadge2.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Second slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_garbadge3.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n              <div class=\"carousel-item\">\n                <img class=\"d-block w-100\" src=\"./assets/img/advrt_garbadge4.jpg\" style=\"height: 300px;width: 300px;\" alt=\"Third slide\">\n              </div>\n            </div>\n          </a>\n          </div>\n          <!--Garbadge Disposal Advertizing slider end-->\n        </div>\n      </div>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"intro\" style=\"height: 698px;text-align: center;background: linear-gradient(to right, #ed213a, #93291e);\">\r\n  <div>\r\n    <img src=\"assets/img/shopping-cart.svg\" alt=\"logo\" style=\"width: 20%;margin-top:170px;\" class=\"animated slow zoomIn\">\r\n    <h1 style=\"color: white;margin-left: 4%;\" class=\"animated slow zoomIn\">Super Daily Needs</h1>\r\n    <p style=\"color: white;margin-left: 4%;\"class=\"animated slow zoomIn\">Marathon towords excellence.</p>\r\n  </div>\r\n</div>\r\n<div class=\"bg-default\" class=\"animated  fadeIn \" id=\"login\" style=\"display:none;\">\r\n  <div  class=\"header py-7 py-lg-5\" style=\"background: linear-gradient(to right, #ed213a, #93291e);\">\r\n      <div class=\"container\">\r\n        <div class=\"header-body text-center mb-7\">\r\n          <div class=\"row justify-content-center\">\r\n            <img src=\"assets/img/shopping-cart.svg\" alt=\"logo\" style=\"width: 18%;\">\r\n            <!-- <div class=\"col-lg-5 col-md-6\">\r\n              <h1 class=\"text-white\">Welcome to AimVo</h1>\r\n              <p class=\"text-lead text-light\">Marathon towords excellence.</p>\r\n            </div> -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  <!-- Page content -->\r\n  <div class=\"container mt--8 pb-5\" >\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-5 col-md-7\">\r\n        <div class=\"card shadow border-0\" >\r\n          <div class=\"card-body px-lg-5 py-lg-5\">\r\n            <form role=\"form\">\r\n              <h1 style=\"margin-bottom: 30px;text-align: center;color: #93291e;\">Super Daily Needs</h1>\r\n              <div class=\"form-group mb-3\">\r\n                <div class=\"input-group input-group-alternative\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\r\n                  </div>\r\n                  <input class=\"form-control\" [(ngModel)]=\"emailInput\" name=\"email\" id=\"email\" placeholder=\"Email\" type=\"email\" autofocus>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <div class=\"input-group input-group-alternative\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>\r\n                  </div>\r\n                  <input class=\"form-control\" [(ngModel)]=\"passwordInput\" name=\"password\" id=\"password\" placeholder=\"Password\" type=\"password\">\r\n                </div>\r\n              </div>\r\n              \r\n              <div class=\"text-center\">\r\n                <button type=\"button\" (click)=\"tryLogin()\" class=\"btn btn-success\">Sign in</button>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-6\">\r\n            <a [routerLink]=\"['/forgotpass']\" class=\"text-light\"><small>Forgot password?</small></a>\r\n          </div>\r\n          <div class=\"col-6 text-right\">\r\n            <a [routerLink]=\"['/creataccount']\" class=\"text-light\"><small>Create new account</small></a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/morning-needs/morning-needs.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/morning-needs/morning-needs.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"header\" style=\"background: linear-gradient(to right, #ed213a, #93291e);\">\n    <div class=\"container-fluid\">\n      <div class=\"header-body\">\n        <div class=\"row align-items-center\">\n            <div class=\"col-4\">\n                <a class=\"mb-0\" [routerLink]=\"['/home']\" class=\"previous\" style=\"padding-left:10px;color:white;font-size: xx-large;\">&#8249;</a>\n              </div>\n              <div class=\"col-8\">\n                <h2 style=\"color: white;text-align: left;\">Morning Needs</h2>\n              </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div><br>\n        <img src=\"/assets/img/texture.jpg\" style=\"height: 10rem;width: 83rem;margin-left: 20px;border: 2px solid rgb(139, 22, 22);\">\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/non-veg/non-veg.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/non-veg/non-veg.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>non-veg works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>offers works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/footer.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/footer.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"footer\" >\r\n    <div class=\"row align-items-center justify-content-xl-between\">\r\n      <div class=\"col-xl-6\">\r\n        <div class=\"copyright text-center text-xl-left text-muted\">\r\n          &copy; 2020 <a href=\"https://www.tectiq.in\" class=\"font-weight-bold ml-1\" target=\"_blank\">Tectiq Technology</a>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-6\">\r\n        <ul class=\"nav nav-footer justify-content-center justify-content-xl-end\">\r\n          <li class=\"nav-item\">\r\n            <a href=\"https://www.tectiq.in\" class=\"nav-link\" target=\"_blank\">Tectiq Technology</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a href=\"https://www.creative-tim.com/presentation\" class=\"nav-link\" target=\"_blank\">About Us</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a href=\"http://blog.creative-tim.com\" class=\"nav-link\" target=\"_blank\">Blog</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a href=\"https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md\" class=\"nav-link\" target=\"_blank\">MIT License</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/header.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/header.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#sidenav-collapse-main\"\r\n        aria-controls=\"sidenav-main\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <!-- Brand -->\r\n      <a class=\"navbar-brand pt-0\" href=\"/\">\r\n        <h2>AimVo</h2>\r\n      </a>\r\n      <!-- User -->\r\n      <ul class=\"nav align-items-center d-md-none\">\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link nav-link-icon\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\r\n            aria-expanded=\"false\">\r\n            <i class=\"ni ni-bell-55\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\" aria-labelledby=\"navbar-default_dropdown_1\">\r\n            <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n            <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\r\n              </span>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\">\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome To AimVo</h6>\r\n            </div>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>My profile</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-settings-gear-65\"></i>\r\n              <span>Settings</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-calendar-grid-58\"></i>\r\n              <span>Activity</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>Support</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-button-power\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a href=\"./index.html\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#sidenav-collapse-main\"\r\n                aria-controls=\"sidenav-main\" aria-expanded=\"false\" aria-label=\"Toggle sidenav\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\"\r\n              aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item  class=\" active>\r\n            <a class=\" nav-link\" href=\" ./dashboard\">\r\n              <i class=\"ni ni-tv-2\"></i> Dashboard\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./users\">\r\n              <i class=\"ni ni-circle-08\"></i> Users\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./qa\">\r\n              <i class=\"fa fa-question\"></i> Question & Answers\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./lan-cate\">\r\n              <i class=\"fa fa-book\"></i> Language Category\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./certification\">\r\n              <i class=\"fa fa-graduation-cap\"></i> Certification\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./refferals\">\r\n              <i class=\"fa fa-asterrisk\"></i> Referral\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link \" href=\"./login\">\r\n              <i class=\"fa fa-power-off\"></i> Logout\r\n            </a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </nav> ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/topmenu.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/topmenu.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-top navbar-expand-md navbar-dark\" id=\"navbar-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Brand -->\r\n      <a class=\"h4 mb-0 text-white text-uppercase d-none d-lg-inline-block\" href=\"./users\">{{title}}</a>\r\n      <!-- Form -->\r\n      <form class=\"navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto\">\r\n        <div class=\"form-group mb-0\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fas fa-search\"></i></span>\r\n            </div>\r\n            <input class=\"form-control\" placeholder=\"Search\" type=\"text\">\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- User -->\r\n      <ul class=\"navbar-nav align-items-center d-none d-md-flex\">\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link pr-0\" href=\"#\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-4-800x800.jpg\">\r\n              </span>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <span class=\"mb-0 text-sm  font-weight-bold\"></span>\r\n              </div>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu dropdown-menu-arrow dropdown-menu-right\">\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>My profile</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-settings-gear-65\"></i>\r\n              <span>Settings</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-calendar-grid-58\"></i>\r\n              <span>Activity</span>\r\n            </a>\r\n            <a href=\"./examples/profile.html\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>Support</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-button-power\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/veg-fruits/veg-fruits.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/veg-fruits/veg-fruits.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>veg-fruits works!</p>\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _veg_fruits_veg_fruits_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./veg-fruits/veg-fruits.component */ "./src/app/veg-fruits/veg-fruits.component.ts");
/* harmony import */ var _morning_needs_morning_needs_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./morning-needs/morning-needs.component */ "./src/app/morning-needs/morning-needs.component.ts");
/* harmony import */ var _non_veg_non_veg_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./non-veg/non-veg.component */ "./src/app/non-veg/non-veg.component.ts");
/* harmony import */ var _garbadge_disposal_garbadge_disposal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./garbadge-disposal/garbadge-disposal.component */ "./src/app/garbadge-disposal/garbadge-disposal.component.ts");
/* harmony import */ var _offers_offers_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./offers/offers.component */ "./src/app/offers/offers.component.ts");
/* harmony import */ var _customer_support_customer_support_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./customer-support/customer-support.component */ "./src/app/customer-support/customer-support.component.ts");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/cart/cart.component.ts");
/* harmony import */ var _creat_account_creat_account_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./creat-account/creat-account.component */ "./src/app/creat-account/creat-account.component.ts");
/* harmony import */ var _forgot_pass_forgot_pass_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./forgot-pass/forgot-pass.component */ "./src/app/forgot-pass/forgot-pass.component.ts");















const routes = [
    // {
    //   path: '',
    //   redirectTo: 'dashboard',
    //   pathMatch : 'full'
    // },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"] },
    { path: 'vegfruits', component: _veg_fruits_veg_fruits_component__WEBPACK_IMPORTED_MODULE_6__["VegFruitsComponent"] },
    { path: 'morningneeds', component: _morning_needs_morning_needs_component__WEBPACK_IMPORTED_MODULE_7__["MorningNeedsComponent"] },
    { path: 'nonveg', component: _non_veg_non_veg_component__WEBPACK_IMPORTED_MODULE_8__["NonVegComponent"] },
    { path: 'garbadgedisposal', component: _garbadge_disposal_garbadge_disposal_component__WEBPACK_IMPORTED_MODULE_9__["GarbadgeDisposalComponent"] },
    { path: 'offers', component: _offers_offers_component__WEBPACK_IMPORTED_MODULE_10__["OffersComponent"] },
    { path: 'customersupport', component: _customer_support_customer_support_component__WEBPACK_IMPORTED_MODULE_11__["CustomerSupportComponent"] },
    { path: 'cart', component: _cart_cart_component__WEBPACK_IMPORTED_MODULE_12__["CartComponent"] },
    { path: 'creataccount', component: _creat_account_creat_account_component__WEBPACK_IMPORTED_MODULE_13__["CreatAccountComponent"] },
    { path: 'forgotpass', component: _forgot_pass_forgot_pass_component__WEBPACK_IMPORTED_MODULE_14__["ForgotPassComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");




let AppComponent = class AppComponent {
    constructor(router, angularFireAuth) {
        this.router = router;
        this.angularFireAuth = angularFireAuth;
        this.title = 'daily-needs';
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared */ "./src/app/shared/index.ts");
/* harmony import */ var _shared_layout_footer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared/layout/footer.component */ "./src/app/shared/layout/footer.component.ts");
/* harmony import */ var _shared_layout_header_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/layout/header.component */ "./src/app/shared/layout/header.component.ts");
/* harmony import */ var _shared_layout_topmenu_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/layout/topmenu.component */ "./src/app/shared/layout/topmenu.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _veg_fruits_veg_fruits_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./veg-fruits/veg-fruits.component */ "./src/app/veg-fruits/veg-fruits.component.ts");
/* harmony import */ var _morning_needs_morning_needs_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./morning-needs/morning-needs.component */ "./src/app/morning-needs/morning-needs.component.ts");
/* harmony import */ var _non_veg_non_veg_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./non-veg/non-veg.component */ "./src/app/non-veg/non-veg.component.ts");
/* harmony import */ var _garbadge_disposal_garbadge_disposal_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./garbadge-disposal/garbadge-disposal.component */ "./src/app/garbadge-disposal/garbadge-disposal.component.ts");
/* harmony import */ var _offers_offers_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./offers/offers.component */ "./src/app/offers/offers.component.ts");
/* harmony import */ var _customer_support_customer_support_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./customer-support/customer-support.component */ "./src/app/customer-support/customer-support.component.ts");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/cart/cart.component.ts");
/* harmony import */ var _creat_account_creat_account_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./creat-account/creat-account.component */ "./src/app/creat-account/creat-account.component.ts");
/* harmony import */ var _forgot_pass_forgot_pass_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./forgot-pass/forgot-pass.component */ "./src/app/forgot-pass/forgot-pass.component.ts");



//firebase























let AppModule = class AppModule {
    constructor(db) {
        this.db = db;
    }
};
AppModule.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] }
];
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"],
            _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__["DashboardComponent"],
            _shared_layout_footer_component__WEBPACK_IMPORTED_MODULE_12__["FooterComponent"], _shared_layout_header_component__WEBPACK_IMPORTED_MODULE_13__["HeaderComponent"], _shared_layout_topmenu_component__WEBPACK_IMPORTED_MODULE_14__["TopmenuComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"], _veg_fruits_veg_fruits_component__WEBPACK_IMPORTED_MODULE_17__["VegFruitsComponent"], _morning_needs_morning_needs_component__WEBPACK_IMPORTED_MODULE_18__["MorningNeedsComponent"], _non_veg_non_veg_component__WEBPACK_IMPORTED_MODULE_19__["NonVegComponent"], _garbadge_disposal_garbadge_disposal_component__WEBPACK_IMPORTED_MODULE_20__["GarbadgeDisposalComponent"], _offers_offers_component__WEBPACK_IMPORTED_MODULE_21__["OffersComponent"], _customer_support_customer_support_component__WEBPACK_IMPORTED_MODULE_22__["CustomerSupportComponent"], _cart_cart_component__WEBPACK_IMPORTED_MODULE_23__["CartComponent"], _creat_account_creat_account_component__WEBPACK_IMPORTED_MODULE_24__["CreatAccountComponent"], _forgot_pass_forgot_pass_component__WEBPACK_IMPORTED_MODULE_25__["ForgotPassComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_3__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_15__["environment"].firebase),
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestoreModule"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuthModule"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__["AngularFireStorageModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_11__["SharedModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/cart/cart.component.scss":
/*!******************************************!*\
  !*** ./src/app/cart/cart.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcnQvY2FydC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/cart/cart.component.ts":
/*!****************************************!*\
  !*** ./src/app/cart/cart.component.ts ***!
  \****************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CartComponent = class CartComponent {
    constructor() { }
    ngOnInit() {
    }
};
CartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cart',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./cart.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./cart.component.scss */ "./src/app/cart/cart.component.scss")).default]
    })
], CartComponent);



/***/ }),

/***/ "./src/app/creat-account/creat-account.component.scss":
/*!************************************************************!*\
  !*** ./src/app/creat-account/creat-account.component.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NyZWF0LWFjY291bnQvY3JlYXQtYWNjb3VudC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/creat-account/creat-account.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/creat-account/creat-account.component.ts ***!
  \**********************************************************/
/*! exports provided: CreatAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatAccountComponent", function() { return CreatAccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CreatAccountComponent = class CreatAccountComponent {
    constructor() { }
    ngOnInit() {
    }
};
CreatAccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-creat-account',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./creat-account.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/creat-account/creat-account.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./creat-account.component.scss */ "./src/app/creat-account/creat-account.component.scss")).default]
    })
], CreatAccountComponent);



/***/ }),

/***/ "./src/app/customer-support/customer-support.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/customer-support/customer-support.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyLXN1cHBvcnQvY3VzdG9tZXItc3VwcG9ydC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/customer-support/customer-support.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/customer-support/customer-support.component.ts ***!
  \****************************************************************/
/*! exports provided: CustomerSupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerSupportComponent", function() { return CustomerSupportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CustomerSupportComponent = class CustomerSupportComponent {
    constructor() { }
    ngOnInit() {
    }
};
CustomerSupportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer-support',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./customer-support.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/customer-support/customer-support.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./customer-support.component.scss */ "./src/app/customer-support/customer-support.component.scss")).default]
    })
], CustomerSupportComponent);



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DashboardComponent = class DashboardComponent {
    constructor() { }
    ngOnInit() {
    }
};
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/dashboard/dashboard.component.scss")).default]
    })
], DashboardComponent);



/***/ }),

/***/ "./src/app/forgot-pass/forgot-pass.component.scss":
/*!********************************************************!*\
  !*** ./src/app/forgot-pass/forgot-pass.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZvcmdvdC1wYXNzL2ZvcmdvdC1wYXNzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/forgot-pass/forgot-pass.component.ts":
/*!******************************************************!*\
  !*** ./src/app/forgot-pass/forgot-pass.component.ts ***!
  \******************************************************/
/*! exports provided: ForgotPassComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPassComponent", function() { return ForgotPassComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ForgotPassComponent = class ForgotPassComponent {
    constructor() { }
    ngOnInit() {
    }
};
ForgotPassComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgot-pass',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./forgot-pass.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/forgot-pass/forgot-pass.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./forgot-pass.component.scss */ "./src/app/forgot-pass/forgot-pass.component.scss")).default]
    })
], ForgotPassComponent);



/***/ }),

/***/ "./src/app/garbadge-disposal/garbadge-disposal.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/garbadge-disposal/garbadge-disposal.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dhcmJhZGdlLWRpc3Bvc2FsL2dhcmJhZGdlLWRpc3Bvc2FsLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/garbadge-disposal/garbadge-disposal.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/garbadge-disposal/garbadge-disposal.component.ts ***!
  \******************************************************************/
/*! exports provided: GarbadgeDisposalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GarbadgeDisposalComponent", function() { return GarbadgeDisposalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let GarbadgeDisposalComponent = class GarbadgeDisposalComponent {
    constructor() { }
    ngOnInit() {
    }
};
GarbadgeDisposalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-garbadge-disposal',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./garbadge-disposal.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/garbadge-disposal/garbadge-disposal.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./garbadge-disposal.component.scss */ "./src/app/garbadge-disposal/garbadge-disposal.component.scss")).default]
    })
], GarbadgeDisposalComponent);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")).default]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let LoginComponent = class LoginComponent {
    constructor(titleService, router) {
        this.titleService = titleService;
        this.router = router;
        this.responseMessage = '';
        this.responseMessageType = '';
        this.selectedVal = 'login';
    }
    ngOnInit() {
        const appTitle = this.titleService.getTitle();
        this.titleService.setTitle('Login');
        setTimeout(() => {
            let intro = document.getElementById('intro');
            let login = document.getElementById('login');
            intro.style.display = 'none';
            login.style.display = 'block';
        }, 4000);
    }
    showMessage(type, msg) {
        this.responseMessageType = type;
        this.responseMessage = msg;
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/morning-needs/morning-needs.component.scss":
/*!************************************************************!*\
  !*** ./src/app/morning-needs/morning-needs.component.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vcm5pbmctbmVlZHMvbW9ybmluZy1uZWVkcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/morning-needs/morning-needs.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/morning-needs/morning-needs.component.ts ***!
  \**********************************************************/
/*! exports provided: MorningNeedsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MorningNeedsComponent", function() { return MorningNeedsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let MorningNeedsComponent = class MorningNeedsComponent {
    constructor(Router) {
        this.Router = Router;
    }
    ngOnInit() {
    }
};
MorningNeedsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
MorningNeedsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-morning-needs',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./morning-needs.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/morning-needs/morning-needs.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./morning-needs.component.scss */ "./src/app/morning-needs/morning-needs.component.scss")).default]
    })
], MorningNeedsComponent);



/***/ }),

/***/ "./src/app/non-veg/non-veg.component.scss":
/*!************************************************!*\
  !*** ./src/app/non-veg/non-veg.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vbi12ZWcvbm9uLXZlZy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/non-veg/non-veg.component.ts":
/*!**********************************************!*\
  !*** ./src/app/non-veg/non-veg.component.ts ***!
  \**********************************************/
/*! exports provided: NonVegComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NonVegComponent", function() { return NonVegComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NonVegComponent = class NonVegComponent {
    constructor() { }
    ngOnInit() {
    }
};
NonVegComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-non-veg',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./non-veg.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/non-veg/non-veg.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./non-veg.component.scss */ "./src/app/non-veg/non-veg.component.scss")).default]
    })
], NonVegComponent);



/***/ }),

/***/ "./src/app/offers/offers.component.scss":
/*!**********************************************!*\
  !*** ./src/app/offers/offers.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29mZmVycy9vZmZlcnMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/offers/offers.component.ts":
/*!********************************************!*\
  !*** ./src/app/offers/offers.component.ts ***!
  \********************************************/
/*! exports provided: OffersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffersComponent", function() { return OffersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let OffersComponent = class OffersComponent {
    constructor() { }
    ngOnInit() {
    }
};
OffersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-offers',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./offers.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./offers.component.scss */ "./src/app/offers/offers.component.scss")).default]
    })
], OffersComponent);



/***/ }),

/***/ "./src/app/shared/index.ts":
/*!*********************************!*\
  !*** ./src/app/shared/index.ts ***!
  \*********************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]
        ],
        declarations: [],
        exports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        ]
    })
], SharedModule);



/***/ }),

/***/ "./src/app/shared/layout/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/layout/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() {
        this.today = Date.now();
    }
    ngOnInit() { }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout-footer',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/footer.component.html")).default
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/shared/layout/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/layout/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderComponent = class HeaderComponent {
    constructor() { }
    ngOnInit() { }
};
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/header.component.html")).default
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/shared/layout/topmenu.component.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/layout/topmenu.component.ts ***!
  \****************************************************/
/*! exports provided: TopmenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopmenuComponent", function() { return TopmenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TopmenuComponent = class TopmenuComponent {
    constructor() { }
    ngOnInit() {
    }
};
TopmenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout-topmenu',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./topmenu.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/layout/topmenu.component.html")).default
    })
], TopmenuComponent);



/***/ }),

/***/ "./src/app/veg-fruits/veg-fruits.component.scss":
/*!******************************************************!*\
  !*** ./src/app/veg-fruits/veg-fruits.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZlZy1mcnVpdHMvdmVnLWZydWl0cy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/veg-fruits/veg-fruits.component.ts":
/*!****************************************************!*\
  !*** ./src/app/veg-fruits/veg-fruits.component.ts ***!
  \****************************************************/
/*! exports provided: VegFruitsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VegFruitsComponent", function() { return VegFruitsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let VegFruitsComponent = class VegFruitsComponent {
    constructor() { }
    ngOnInit() {
    }
};
VegFruitsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-veg-fruits',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./veg-fruits.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/veg-fruits/veg-fruits.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./veg-fruits.component.scss */ "./src/app/veg-fruits/veg-fruits.component.scss")).default]
    })
], VegFruitsComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyBDvWXonkAo55p58KAAS9YCTkAqdryppeA",
        authDomain: "aimvotectiq.firebaseapp.com",
        databaseURL: "https://aimvotectiq.firebaseio.com",
        projectId: "aimvotectiq",
        storageBucket: "aimvotectiq.appspot.com",
        messagingSenderId: "189139059786",
        appId: "1:189139059786:web:3e75b88fbc745d2ad312b3",
        measurementId: "G-0C4Z453M77"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!**********************************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node ./src/main.ts ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/paragraj/Documents/Snehal/daily-needs/node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node */"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node&sockPath=/sockjs-node");
module.exports = __webpack_require__(/*! /home/paragraj/Documents/Snehal/daily-needs/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map