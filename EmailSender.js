require('dotenv').config();
const nodemailer = require('nodemailer');
const Email = require('email-templates');
var orderConfirmationEmailSubject = "Your Super Daily Need order has been confirmed"

let transporter = nodemailer.createTransport({
    service: process.env.SERVICE,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
});

const email = new Email({
    views: { root: './emailTemplates', options: { extension: 'ejs' } },
    message: {
        from: 'Super Daily Need <' + process.env.EMAIL + '>',
        subject: orderConfirmationEmailSubject
    },
    preview: false,
    send: true,
    transport: transporter
})

exports.sendOrderConfirmationEmail = function (emailData) {
    email.send({
        template: 'orderConfirmation',
        message: {
            to: emailData.emailId,
        },
        locals: {
            username: emailData.username,
            date: emailData.date,
            total: emailData.total,
            address: emailData.address,
            products: emailData.products
        }
    }).then(console.log)
        .catch(console.error)
}